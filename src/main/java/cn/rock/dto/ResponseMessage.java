package cn.rock.dto;

/**
 * @author rock
 * @date 2017/7/7
 */
public class ResponseMessage<T> {
    private T data;

    public ResponseMessage() {
    }

    public static <T> ResponseMessage<T> success(T data) {
        ResponseMessage<T> responseMessage = new ResponseMessage();
        responseMessage.setData(data);
        return responseMessage;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
