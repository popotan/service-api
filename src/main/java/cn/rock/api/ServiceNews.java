package cn.rock.api;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author rock
 * @date 2017/7/7
 */
@FeignClient(value = "service-news")
public interface ServiceNews {
    @GetMapping("/wow")
    String
    wow(@RequestParam("wow") String wow);

    @GetMapping("/cnm")
    String cnm(@RequestParam("cnm") String cnm);
}
