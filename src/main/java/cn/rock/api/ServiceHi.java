package cn.rock.api;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author rock
 * @date 2017/7/7
 */
@FeignClient(value = "service-hi")
public interface ServiceHi {
    @GetMapping("/rock")
    String rock(@RequestParam("rock") String rock);

    @GetMapping("/cnm")
    String cnm(@RequestParam("cnm") String cnm);
}
