package cn.rock.api;

import cn.rock.dto.ResponseMessage;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author rock
 * @date 2017/7/7
 */
@FeignClient(value = "service-token")
public interface ServiceToken {

    @RequestMapping("/token/getTokenPhone")
    String getTokenPhone(String token);

    /**
     * 刷新jwt
     */
    @RequestMapping("/token/refresh")
    ResponseEntity<?> refreshToken();


    /**
     * 获取新的token
     */
    @RequestMapping(method = RequestMethod.POST, path = "/token/getNewToken")
    String getNewToken(String token);

    /**
     * 网关校验jwt使用
     */
    @RequestMapping("/token/verify")
    Boolean verifyToken(String token);

    /**
     * 登出
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, path = "/logout")
    ResponseMessage logout();

    /**
     * 踢掉在线用户
     *
     * @return
     */
    @RequestMapping(method = RequestMethod.POST, path = "/kickOnlieUser")
    ResponseMessage kickOnlieUser(Long userId);
}
